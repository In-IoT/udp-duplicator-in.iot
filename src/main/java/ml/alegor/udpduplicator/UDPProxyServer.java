package ml.alegor.udpduplicator;


import java.io.BufferedReader;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

import ml.alegor.udpduplicator.Config.ConfigClass;


public class UDPProxyServer {
	DatagramSocket serverSocket;
	int listenPort;
	String destinationAddress;
	int destinationPort;
	int result = -1;
	public UDPProxyServer() {
		
	}
	
	public void start() {
		
		serverSocket= null;
        try {
            serverSocket = new DatagramSocket(listenPort);
            serverSocket.setReceiveBufferSize(8192);
            serverSocket.setSendBufferSize(8192);
            serverSocket.setReuseAddress(true);
        } catch (SocketException ex) {
            Logger.getLogger(UDPProxyServer.class.getName()).log(Level.SEVERE, null, ex);
        }        
        while (true) {
            try {
            	
            	
//                System.out.println(proxy.getPort());
//                System.out.println(proxy.getHost());
                byte[] receiveData = new byte[8192];
                DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
//                serverSocket.rece
                serverSocket.receive(receivePacket);
                
            	Random r = new Random();
                int Low = 0;
                int High = ConfigClass.forwardAddressAndPort.size();
                if(High<1) {
                	System.out.println("No instances of the destination service could be found");
                } else {
                	//return;
                	System.out.println("packet received");
	                result = r.nextInt(High-Low) + Low;
	                
	
	                destinationAddress = ConfigClass.forwardAddressAndPort.get(result).getAddress();
	                destinationPort = ConfigClass.forwardAddressAndPort.get(result).getPort();
	                UDPProxy udp=new UDPProxy(receivePacket,serverSocket,destinationAddress,listenPort,destinationPort);
                }
                
            } catch (IOException ex) {
                Logger.getLogger(UDPProxyServer.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
		
	}

//    public UDPProxyServer(String host, int local, int remote) {
//    	
//
//    }

}