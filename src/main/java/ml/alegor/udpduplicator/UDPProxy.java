package ml.alegor.udpduplicator;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class UDPProxy {

    public UDPProxy(DatagramPacket receivePacket, DatagramSocket serverSocket, String host, int local, int remote) {
        DatagramSocket proxySocket = null;
    try {
        proxySocket = new DatagramSocket();
        proxySocket.setReceiveBufferSize(8192);
        proxySocket.setSendBufferSize(8192);
        proxySocket.setReuseAddress(true);
    } catch (SocketException ex) {
        Logger.getLogger(UDPProxyServer.class.getName()).log(Level.SEVERE, null, ex);
    }        
        try {
            //ip of client
            InetAddress IPAddress = receivePacket.getAddress();

            //port of client
            int port = receivePacket.getPort();

            //actual server ip
            InetAddress pIPAddress = InetAddress.getByName(host);

            //convert
            byte[] real = new byte[receivePacket.getLength()];
            System.arraycopy(receivePacket.getData(), 0, real, 0, receivePacket.getLength());

            //Packet received
//            System.out.println("Packet from Client: " + receivePacket.getAddress().toString() + " Port: " + receivePacket.getPort() + " To: " + local + " Data: " + new String(real));

            //packet sent to actual server            
            DatagramPacket sendPacket = new DatagramPacket(real, real.length, pIPAddress, remote);

            //send packet
            proxySocket.send(sendPacket);

            //log that
//            System.out.println("Sent to Server: " + sendPacket.getAddress() + " Port: " + sendPacket.getPort());

            //a new receive data for server response
            byte[] receiveData = new byte[8192];

            //servers response packet
            DatagramPacket preceivePacket = new DatagramPacket(receiveData, receiveData.length);

            //receive that packet from server
            proxySocket.receive(preceivePacket);

            //convert
            byte[] preal = new byte[preceivePacket.getLength()];
            System.arraycopy(preceivePacket.getData(), 0, preal, 0, preceivePacket.getLength());

            //log that
//            System.out.println("Response From Server: " + preceivePacket.getAddress().toString() + " Port: " + preceivePacket.getPort() + " Data: " + new String(preal));

            //packet for response to send back to the client
            DatagramPacket psendPacket = new DatagramPacket(preal, preal.length, IPAddress, port);

            //send packet back to client
            serverSocket.send(psendPacket);

//            System.out.println("Response Sent To Client: " + psendPacket.getAddress().toString() + " Port: " + psendPacket.getPort());

        } catch (IOException ex) {
            Logger.getLogger(UDPProxyServer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}