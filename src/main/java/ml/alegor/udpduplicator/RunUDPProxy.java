package ml.alegor.udpduplicator;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;

import ml.alegor.udpduplicator.Config.ConfigClass;
import ml.alegor.udpduplicator.Config.EurekaMethods;
import ml.alegor.udpduplicator.Config.FileResourceLoader;
import ml.alegor.udpduplicator.Config.IConfig;
import ml.alegor.udpduplicator.Config.IResourceLoader;
import ml.alegor.udpduplicator.Config.ResourceLoaderConfig;



class RunUDPProxy {
    public static void main(String[] args) throws Exception {
    	
    	checkIfConfigFileExists();
		File defaultConfigurationFile = defaultConfigFile();
        IResourceLoader filesystemLoader = new FileResourceLoader(defaultConfigurationFile);
		
		final IConfig config = new ResourceLoaderConfig(filesystemLoader);
    	
		ConfigClass.eurekaAddress = config.getProperty("eurekaAddress");
		ConfigClass.eurekaUsername = config.getProperty("eurekaUsername");
		ConfigClass.eurekaPassword = config.getProperty("eurekaPassword");
		
		int listenPort = Integer.parseInt(config.getProperty("listenPort"));
    	
    	new Thread(new Runnable() {

			public void run() {

				while (true) {
					try {
						
						EurekaMethods eurekaMethods = new EurekaMethods();
						eurekaMethods.getMQTTInstances();
						Thread.sleep(50000);
						
						}

						catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}}
		).start();
//    	Thread.sleep(5000);
//    	UDPProxyServer udpProxy =  new UDPProxyServer("localhost", listenPort, 5683);
    	UDPProxyServer udpProxy =  new UDPProxyServer();
    	udpProxy.listenPort = listenPort;
    	udpProxy.start();

    }
    
    
private static File defaultConfigFile() {
		
		String configPath = System.getProperty("user.dir");
        //String configPath = System.getProperty("moquette.path", null);
        System.out.println("This is the configPath " + configPath);
        return new File(configPath, IConfig.DEFAULT_CONFIG);
    }
	
	private static void checkIfConfigFileExists() {

		String filename = System.getProperty("user.dir")+ File.separator + "config" + File.separator + "proxy.conf";
		File file = new File(filename);
		if (!file.exists()) {
			file.mkdirs();
			file.delete();
			try {
				file.createNewFile();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			try (Writer writer = new BufferedWriter(
					new OutputStreamWriter(new FileOutputStream(filename), "utf-8"))) {
				writer.write("listenPort 5001\n" + 
						"\n" + 
						"eurekaAddress http://localhost:8761\n" + 
						"\n" + 
						"eurekaUsername inIoTTest\n" + 
						"eurekaPassword lucasabbadeWare");
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
}